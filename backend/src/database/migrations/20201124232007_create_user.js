exports.up = function (knex) {
    return knex.schema.createTable('user', function (table) {
        table.string('id').primary();
        table.string('name').notNullable();
        table.string('date').notNullable();
        table.string('document_RG').notNullable();
        table.string('document_CPF').notNullable();
        table.string('nickname').notNullable();
        table.string('email').notNullable();
        table.string('password').notNullable();
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('user');
};
