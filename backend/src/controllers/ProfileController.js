const connection = require('../database/connection');

module.exports = {
    async findOne(req, res) {
        const user_id = req.headers.authorization;

        const unicorn = await connection('unicorn')
            .where('user_id', user_id)
            .select('*');

        return res.json(unicorn);
    }
}
