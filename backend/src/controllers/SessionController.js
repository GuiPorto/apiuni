const connection = require('../database/connection');

module.exports = {
    async store(req, res) {
        const  { id, password } = req.body;

        const user = await  connection('user')
            .where('id', id )
            .where('password' , password)
            .select('name')
            .first();

        if (!user) {
            return res.status(400).json({error: 'User not found'});
        }

        return res.json(user);
    }
}
